package com.harish.sportskeeda.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AlgoMeta {

    @SerializedName("recency")
    @Expose
    var recency: String? = null
    @SerializedName("sk_reads")
    @Expose
    var skReads: String? = null
    @SerializedName("section_breaking")
    @Expose
    var sectionBreaking: String? = null
    @SerializedName("section_live")
    @Expose
    var sectionLive: String? = null
    @SerializedName("number_of_comments")
    @Expose
    var numberOfComments: String? = null
    @SerializedName("sk_live_traffic")
    @Expose
    var skLiveTraffic: String? = null
    @SerializedName("sport_rank")
    @Expose
    var sportRank: Int? = null
    @SerializedName("event_rank")
    @Expose
    var eventRank: Int? = null
    @SerializedName("author_rank")
    @Expose
    var authorRank: Int? = null
    @SerializedName("type_rank")
    @Expose
    var typeRank: Int? = null
    @SerializedName("personal_score")
    @Expose
    var personalScore: Int? = null

}
