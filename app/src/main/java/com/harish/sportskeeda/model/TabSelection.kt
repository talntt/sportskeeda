package com.harish.sportskeeda.model


class TabSelection {
    var text: String? = null
    var isSelected: Boolean? = null
}
