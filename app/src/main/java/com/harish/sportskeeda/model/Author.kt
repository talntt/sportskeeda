package com.harish.sportskeeda.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Author {

    @SerializedName("name")
    @Expose
    var name: String? = null

}
