package com.harish.sportskeeda.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Sports {

    @SerializedName("feed")
    @Expose
    var feed: List<Feed>? = null
    @SerializedName("count")
    @Expose
    var count: List<Any>? = null

}
