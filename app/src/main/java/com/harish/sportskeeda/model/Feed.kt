package com.harish.sportskeeda.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Feed {

    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String? = null
    @SerializedName("word_count")
    @Expose
    var wordCount: Int? = null
    @SerializedName("author")
    @Expose
    var author: Author? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("modified_date")
    @Expose
    var modifiedDate: String? = null
    @SerializedName("permalink")
    @Expose
    var permalink: String? = null
    @SerializedName("published_date")
    @Expose
    var publishedDate: String? = null
    @SerializedName("read_count")
    @Expose
    var readCount: String? = null
    @SerializedName("comment_count")
    @Expose
    var commentCount: Int? = null
    @SerializedName("live_traffic")
    @Expose
    var liveTraffic: Int? = null
    @SerializedName("rank")
    @Expose
    var rank: Double? = null
    @SerializedName("post_tag")
    @Expose
    var postTag: List<String>? = null
    @SerializedName("algo_meta")
    @Expose
    var algoMeta: AlgoMeta? = null
    @SerializedName("index")
    @Expose
    var index: Int? = null
    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("excerpt")
    @Expose
    var excerpt: String? = null

}
