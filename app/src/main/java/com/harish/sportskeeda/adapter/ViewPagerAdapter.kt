package com.harish.sportskeeda.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.harish.sportskeeda.ui.feeds.All


class ViewPagerAdapter internal constructor(fm: FragmentManager, list: ArrayList<String>) : FragmentStatePagerAdapter(fm) {
    var list: ArrayList<String>? = null


    fun updateList(listChanged: ArrayList<String>) {
        this.list = listChanged
        notifyDataSetChanged()
    }

    init {
        this.list = list
    }

    override fun getCount(): Int {
        return list!!.size
    }

    override fun getItem(i: Int): Fragment {
        return All.newInstance(i, list!!.get(i))
    }

    override fun getPageTitle(position: Int): CharSequence {
        return list!!.get(position)
    }
}