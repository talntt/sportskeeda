package com.harish.sportskeeda.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.harish.sportskeeda.R
import com.harish.sportskeeda.model.Feed
import com.squareup.picasso.Picasso

internal class FeedsAdapter(list: List<Feed>, context: Context) : RecyclerView.Adapter<FeedsAdapter.MyViewHolder>() {

    var list: List<Feed>? = null
    var context: Context? = null

    init {
        this.list = list
        this.context = context
    }

    fun updateList(list: List<Feed>) {
        this.list = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.list, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val feed = list?.get(position)
        holder.title?.text = feed?.title
        holder.name?.text = feed?.author?.name
        if (!TextUtils.isEmpty(feed?.thumbnail))
            Picasso.get().load(feed?.thumbnail).placeholder(R.drawable.placeholder).into(holder.image)
        else
            Picasso.get().load(R.drawable.placeholder).into(holder.image)
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView? = null
        var title: TextView? = null
        var image: ImageView? = null

        init {
            name = itemView.findViewById(R.id.name)
            title = itemView.findViewById(R.id.title)
            image = itemView.findViewById(R.id.image_view)
        }
    }
}