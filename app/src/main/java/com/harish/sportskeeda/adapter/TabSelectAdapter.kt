package com.harish.sportskeeda.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.harish.sportskeeda.R
import com.harish.sportskeeda.model.TabSelection
import com.harish.sportskeeda.ui.feeds.IMainView

internal class TabSelectAdapter(list: List<TabSelection>, context: Context, iView: IMainView) :
    RecyclerView.Adapter<TabSelectAdapter.MyViewHolder>() {

    var list: List<TabSelection>? = null
    var context: Context? = null
    var iView: IMainView? = null
    var isDisabled = true
    var hasTrue: ArrayList<Boolean>? = null

    init {
        this.iView = iView
        this.list = list
        this.context = context
    }

    fun updateListAfterChanges(list: List<TabSelection>) {
        this.list = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.list_tab, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list?.get(position)
        holder.text?.text = item?.text
        holder.isSelection?.isChecked = item?.isSelected!!

        holder.isSelection?.setOnCheckedChangeListener { compoundButton, b ->
            list?.get(holder.adapterPosition)?.isSelected = b
            iView?.onClick(position, list!!)

            hasTrue = ArrayList()
            for (i in 0..list?.size!!.minus(1)) {
               if(list?.get(i)?.isSelected!!) {
                   hasTrue?.add(true)
               }
            }

            if(hasTrue?.size!! <= 1) {
                compoundButton.isChecked = true
                list?.get(holder.adapterPosition)?.isSelected = true
                iView?.onClick(position, list!!)
            }
        }

        holder.isSelection?.isEnabled = isDisabled

        if (item.text!!.equals("all")) {
            holder.isSelection?.visibility = View.GONE
        } else {
            holder.isSelection?.visibility = View.VISIBLE
        }
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var text: TextView? = null
        var isSelection: CheckBox? = null

        init {
            text = itemView.findViewById(R.id.text)
            isSelection = itemView.findViewById(R.id.checkbox)
        }
    }
}