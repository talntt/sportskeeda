package com.harish.sportskeeda.ui.feeds

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.harish.sportskeeda.R
import com.harish.sportskeeda.adapter.ViewPagerAdapter
import com.harish.sportskeeda.model.Feed
import org.greenrobot.eventbus.EventBus
import com.google.android.material.bottomsheet.BottomSheetBehavior
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.harish.sportskeeda.adapter.TabSelectAdapter
import com.harish.sportskeeda.model.ChangeType
import com.harish.sportskeeda.model.TabSelection
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import com.harish.sportskeeda.model.SearchType
import java.lang.Exception


class DashBoardActivity : AppCompatActivity(), IMainView, View.OnClickListener {

    var viewPager: ViewPager? = null
    var tabLayout: TabLayout? = null
    var viewModel: DashBoardViewModel? = null
    var setting: ImageView? = null
    var search: ImageView? = null
    var down: ImageView? = null
    var searchEdit: EditText? = null
    var layoutBottomSheet: RelativeLayout? = null
    var sheetBehavior: BottomSheetBehavior<*>? = null
    var mRecyclerView: RecyclerView? = null
    var tabSelectedlist: List<TabSelection>? = null
    internal var tabSelectAdapter: TabSelectAdapter? = null
    var viewPagerAdapter: ViewPagerAdapter? = null
    var isSearchShow: Boolean? =false
    var list: ArrayList<String>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        viewModel = ViewModelProviders.of(this).get(DashBoardViewModel::class.java)
        tabLayout = findViewById(R.id.tab_layout)
        viewPager = findViewById(R.id.view_pager)
        setting = findViewById(R.id.settings)
        search = findViewById(R.id.search)
        searchEdit = findViewById(R.id.search_edit_frame)
        down = findViewById(R.id.down)
        layoutBottomSheet = findViewById(R.id.bottom_sheet_layout)
        mRecyclerView = findViewById(R.id.recycler_view_tabs_select)
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet)
        invokeBottomSheetCallBack()
        setting?.setOnClickListener(this)
        down?.setOnClickListener(this)
        search?.setOnClickListener(this)

        val liveData = viewModel?.getNewsFeeds()
        liveData?.observe(this, Observer<List<Feed>> {
            if (viewPager?.adapter == null) {
                setBottomList(it)
            }
            if (mRecyclerView?.adapter == null) {
                tabSelectAdapter = TabSelectAdapter(tabSelectedlist!!, applicationContext, this)
                mRecyclerView?.adapter = tabSelectAdapter
            }
            EventBus.getDefault().post(it)
        })

        viewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }
            override fun onPageSelected(position: Int) {
                val type = ChangeType(list?.get(position)!!)
                EventBus.getDefault().post(type)
                hideKeyboard()
            }
            override fun onPageScrollStateChanged(state: Int) {
            }
        })

        searchEdit?.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }
            override fun afterTextChanged(s: Editable) {
                val key = SearchType(s.toString())
                EventBus.getDefault().post(key)
            }
        })

    }

    fun invokeSearchView() {

        if(!(isSearchShow!!)) {
            searchEdit?.visibility == View.VISIBLE
            search?.setImageResource(R.drawable.ic_close)
        } else {
            searchEdit?.visibility == View.GONE
            search?.setImageResource(R.drawable.ic_search)
        }
        isSearchShow = !(isSearchShow!!)
    }

    fun invokeBottomSheetCallBack() {
        sheetBehavior!!.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // React to state change
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {

                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {

                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        if(tabSelectAdapter != null) {
                            tabSelectAdapter?.updateListAfterChanges(tabSelectedlist!!)
                            settingUpViewPager(tabSelectedlist!!)
                        }
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }
        })
    }

    override fun getlist(): MutableLiveData<List<Feed>> {
        return viewModel?.getNewsFeeds()!!
    }

    override fun setUpViewPaget(list: ArrayList<String>) {
        this.list = list
        if(viewPagerAdapter == null) {
            viewPager?.offscreenPageLimit = list.size
            viewPagerAdapter = ViewPagerAdapter(supportFragmentManager, list)
            viewPager?.adapter = viewPagerAdapter
            tabLayout?.setupWithViewPager(viewPager)
        } else {
            viewPagerAdapter?.updateList(list)
            viewPager?.setCurrentItem(0, true)
        }
    }

    override fun onClick(view: View?) {

        when(view) {
            setting -> {
                if (sheetBehavior?.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior?.setState(BottomSheetBehavior.STATE_EXPANDED)
                }
            }
            down -> {
                if (sheetBehavior?.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior?.setState(BottomSheetBehavior.STATE_COLLAPSED)
                }
            }
            search -> {
                invokeSearchView()
            }
        }
    }

    override fun onClick(position: Int, list: List<TabSelection>) {
        tabSelectedlist = list
    }

    fun setBottomList(list: List<Feed>) {
        tabSelectedlist = ArrayList()
        val tabList =  viewModel?.getCountofFragment(list)
        for(type in tabList!!) {
            val tabSelection = TabSelection()
            tabSelection.text = type
            tabSelection.isSelected = true
            (tabSelectedlist as ArrayList<TabSelection>).add(tabSelection)
        }
        settingUpViewPager(tabSelectedlist!!)
    }

    fun settingUpViewPager(tabSelectedlist: List<TabSelection>) {
        setUpViewPaget(viewModel?.getCountofFragments(tabSelectedlist)!!)
    }

    fun hideKeyboard() {
        try {
            val view = this.currentFocus
            view?.let { v ->
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                imm?.let { it.hideSoftInputFromWindow(v.windowToken, 0) }
            }
        } catch (e : Exception) {
            Log.e("e", e.toString())
        }
    }
}
