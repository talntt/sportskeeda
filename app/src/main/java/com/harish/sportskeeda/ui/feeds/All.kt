package com.harish.sportskeeda.ui.feeds

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.harish.sportskeeda.R
import com.harish.sportskeeda.adapter.FeedsAdapter
import com.harish.sportskeeda.model.Feed
import com.harish.sportskeeda.utils.Utils
import com.harish.sportskeeda.utils.Utils.Companion.TAB_TWO
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import com.harish.sportskeeda.utils.IClickLongPressEvents
import android.content.Intent
import com.harish.sportskeeda.model.ChangeType
import com.harish.sportskeeda.model.SearchType



class All : Fragment() {

    var mContext: Context? = null
    var mRecyclerView: RecyclerView? = null
    internal var mAdapter: FeedsAdapter? = null
    var position: Int? = 0
    var type: String? = null
    var actualList: List<Feed>? = null;
    var mainList: List<Feed>? = null;

    companion object {
        fun newInstance(sectionNumber: Int, value: String): All {
            val fragment = All()
            val args = Bundle()
            args.putInt(Utils.ARG_SECTION_NUMBER, sectionNumber)
            args.putString(Utils.GLOBAL_LIST, value)
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
        retainInstance = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        position = getArguments()?.getInt(Utils.ARG_SECTION_NUMBER)
        type = getArguments()?.getString(Utils.GLOBAL_LIST)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_all, container, false)
        init(view)
        return view
    }

    private fun init(view: View?) {
        mRecyclerView = view?.findViewById(R.id.recycler_view)
        mRecyclerView?.layoutManager = StaggeredGridLayoutManager(TAB_TWO, StaggeredGridLayoutManager.VERTICAL)
        mRecyclerView?.addOnItemTouchListener(
            Utils.Companion.RecyclerTouchListener(mContext!!,
                mRecyclerView!!, object : IClickLongPressEvents {
                    override fun onClick(view: View, position: Int) {
                        //can alson performs click event
                    }

                    override fun onLongClick(view: View, position: Int) {
                        shareTextUrl(actualList?.get(position)?.title!!, actualList?.get(position)?.permalink!!)
                    }
                })
        )
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun getDataFromSubscriber(list: List<Feed>) {
        mainList = list
        actualList = getListByType(type, list)
        if (mAdapter == null) {
            mAdapter = FeedsAdapter(actualList!!, mContext!!)
            mRecyclerView?.adapter = mAdapter
        } else {
            mAdapter?.updateList(actualList!!)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun changeList(type: ChangeType) {
        actualList = getListByType(type.type!!, mainList!!)
        mAdapter?.updateList(actualList!!)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun searchList(type: SearchType) {
        val temp = ArrayList<Feed>()
        for (d in actualList!!) {
            if (d.title?.toLowerCase()?.contains(type.key?.toLowerCase()!!)!!) {
                temp.add(d)
            }
        }
        mAdapter?.updateList(temp)
    }

    //this should be comes under presenter, for time being, I've return it here.
    fun getListByType(type: String?, list: List<Feed>): List<Feed> {
        val actualList = ArrayList<Feed>()
        if (type.equals("all", true)) {
            return list
        } else {
            for (feed in list) {
                if (feed.type.equals(type)) {
                    actualList.add(feed)
                }
            }
            return actualList
        }
    }

    private fun shareTextUrl(title: String, url: String) {
        val share = Intent(Intent.ACTION_SEND)
        share.type = "text/plain"
        share.putExtra(Intent.EXTRA_SUBJECT, title)
        share.putExtra(Intent.EXTRA_TEXT, url)
        startActivity(Intent.createChooser(share, "Share to..."))
    }

}