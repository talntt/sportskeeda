package com.harish.sportskeeda.ui.feeds

import androidx.lifecycle.MutableLiveData
import com.harish.sportskeeda.model.Feed
import com.harish.sportskeeda.model.TabSelection

interface IMainView {

    fun getlist(): MutableLiveData<List<Feed>>

    fun setUpViewPaget(list: ArrayList<String>)

    fun onClick(position: Int, list: List<TabSelection>)

}
