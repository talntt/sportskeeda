package com.harish.sportskeeda.ui.feeds

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.harish.sportskeeda.model.Feed
import com.harish.sportskeeda.model.Sports
import com.harish.sportskeeda.model.TabSelection
import com.harish.sportskeeda.network.RestFullServices

class DashBoardViewModel: ViewModel(), INetworkCallBack {

    private var newsFeeds: MutableLiveData<List<Feed>>? = null

    fun getNewsFeeds(): MutableLiveData<List<Feed>> {
        if(newsFeeds == null) {
            newsFeeds = MutableLiveData()
            makeNewsFeedRequest()
        }
        return newsFeeds!!
    }

    fun makeNewsFeedRequest() {
        RestFullServices.makeFeedRequest(this)
    }

    fun getCountofFragments(list: List<TabSelection>): ArrayList<String> {
        val arrayCount = ArrayList<String>()
        arrayCount.add("all")
        for (feed in list) {
            if (!(arrayCount.contains(feed.text)) && feed.isSelected!!) arrayCount.add(feed.text!!)
        }
        return arrayCount
    }

    fun getCountofFragment(list: List<Feed>): ArrayList<String> {
        val arrayCount = ArrayList<String>()
        arrayCount.add("all")
        for (feed in list) {
            if (!(arrayCount.contains(feed.type))) arrayCount.add(feed.type!!)
        }
        return arrayCount
    }


    override fun onCleared() {
        super.onCleared()
    }

    override fun onComplete(sports: Sports) {
        newsFeeds?.value = sports.feed
    }

    override fun onError(msg: String) {
        Log.e("onError", msg)
    }
}