package com.harish.sportskeeda.ui.feeds

import com.harish.sportskeeda.model.Feed
import com.harish.sportskeeda.model.Sports

interface INetworkCallBack {

    /**
     * Due to one API call we hardcode the response as actual object,
     * If multiple api calls is there then we need to make  feed model class to extend BaseResponseModel
     * If all response classes extends single class means we can use it everywhere
     */

    fun onComplete(sports: Sports)

    fun onError(msg: String)

}