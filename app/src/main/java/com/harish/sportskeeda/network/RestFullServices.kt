package com.harish.sportskeeda.network

import com.harish.sportskeeda.model.Sports
import com.harish.sportskeeda.ui.feeds.INetworkCallBack
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RestFullServices {

    companion object {

        private val url = "http://data.sportskeeda.com/"

        private fun getRetrofitClient(): ApiManager {
            val retrofit = Retrofit.Builder().baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            val manager = retrofit.create(ApiManager::class.java)
            return manager
        }

        fun makeFeedRequest(callback: INetworkCallBack) {
            getRetrofitClient().getFeeds().enqueue(object : Callback<Sports> {
                override fun onResponse(call: Call<Sports>, response: Response<Sports>) {
                    if (response.isSuccessful) {
                        callback.onComplete(response.body()!!)
                    }
                }

                override fun onFailure(call: Call<Sports>, t: Throwable) {
                    callback.onError(t.message!!)
                }
            })
        }
    }

}