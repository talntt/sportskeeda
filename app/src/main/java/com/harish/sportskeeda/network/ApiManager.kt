package com.harish.sportskeeda.network

import com.harish.sportskeeda.model.Sports
import retrofit2.Call
import retrofit2.http.GET

interface ApiManager {
    @GET("data/2.5/forecast?{id}&{appid}")
    fun getFeeds(): Call<Sports>
}