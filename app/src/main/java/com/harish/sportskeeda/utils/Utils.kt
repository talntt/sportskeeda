package com.harish.sportskeeda.utils

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent
import androidx.recyclerview.widget.RecyclerView
import com.harish.sportskeeda.ui.feeds.All

class Utils {
    companion object {
        val TAB_TWO = 1
        val ARG_SECTION_NUMBER = "section_number"
        val GLOBAL_LIST = "global_list"

        internal class RecyclerTouchListener(context: Context, recycleView: RecyclerView, private val clicklistener: IClickLongPressEvents?) : RecyclerView.OnItemTouchListener {
            private val gestureDetector: GestureDetector

            init {
                gestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
                    override fun onSingleTapUp(e: MotionEvent): Boolean {
                        return true
                    }

                    override fun onLongPress(e: MotionEvent) {
                        val child = recycleView.findChildViewUnder(e.x, e.y)
                        if (child != null && clicklistener != null) {
                            clicklistener.onLongClick(child, recycleView.getChildAdapterPosition(child))
                        }
                    }
                })
            }

            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                val child = rv.findChildViewUnder(e.x, e.y)
                if (child != null && clicklistener != null && gestureDetector.onTouchEvent(e)) {
                    clicklistener.onClick(child, rv.getChildAdapterPosition(child))
                }

                return false
            }

            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {

            }

            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

            }
        }


    }
}