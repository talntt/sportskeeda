package com.harish.sportskeeda.utils

import android.view.View

interface IClickLongPressEvents {
    fun onClick(view: View, position: Int)
    fun onLongClick(view: View, position: Int)
}
